This project contains the kubernetes exercises from "Principios y herramientas de desarrollo" subject.

The project structure is as following:

* hw-02: Exercises responses for hw-02. Delivery date: 18/11/2020
    * resources: folder containing the files used in the exercises.
        * md_images: contains images used in the exercise response.
        * 1: Kubernetes scripts for the 1st question
        * 2: Kubernetes scripts for the 2nd question
        * 3: Kubernetes scripts for the 3rd question
        * 4: Kubernetes scripts for the 4th question
        * 5: Kubernetes scripts for the 5th question
        
* hw-03: Exercises responses for hw-03. Delivery date: 22/11/2020
    * resources: folder containing the files used in the exercises.
        * md_images: contains images used in the exercise response.
        * 1: Files for the 1st question
        * 2: Kubernetes scripts for the 2nd question
        * 3: Kubernetes scripts for the 3rd question