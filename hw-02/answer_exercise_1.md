The following yml defines a pod with the characteristics required in the exercise:
```yaml
apiVersion: v1
kind: Pod
metadata:
  name: nginx-demo
  labels:
    app: nginx-server
spec:
  containers:
    - name: nginx-demo
      image: nginx:1.19.4
      resources:
        requests:
          memory: "256Mi"
          cpu: "100m"
        limits:
          memory: "256Mi"
          cpu: "100m"
```
Then in the cli, execute the command ```kubectl create -f nginx-pod.yml```.

**¿Cómo puedo obtener las últimas 10 líneas de la salida estándar (logs generados por la aplicación)?**

```kubectl logs --tail=10 nginx-demo```

**¿Cómo podría obtener la IP interna del pod? Aporta capturas para indicar el proceso que seguirías.**

```kubectl get pods -o wide```

![Alt](./resources/md_images/1-pods-wide.png "Command output pod wide")

The check the column IP.

you can also use the command ```kubectl describe pod nginx-demo``` and check the IP field.

![Alt](./resources/md_images/1-pod-describe.png "Command output pod describe")

**¿Qué comando utilizarías para entrar dentro del pod?**

```kubectl exec -it nginx-demo -- sh```

**Necesitas visualizar el contenido que expone NGINX, ¿qué acciones debes llevar a cabo?**

First you need to create a port-forward from host port to container port inside the pod.

```kubectl port-forward pod/nginx-demo 8888:80```

![Alt](./resources/md_images/1-port-forward.png "Command output port forward")

Then you can go to web browser and input the URL "localhost:8888".

![Alt](./resources/md_images/1-port-forward-web.png "Command output port forward")

**Indica la calidad de servicio (QoS) establecida en el pod que acabas de crear. ¿Qué lo has mirado?**

Execute the command ```kubectl describe pod nginx-demo``` and go to the "QoS Class".

![Alt](./resources/md_images/1-QoS.png "Command output port forward")

In this case the QoS for the pod is "Guaranteed".