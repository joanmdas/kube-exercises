The following yml defines a replica set for nginx with 3 replicas:
```yaml
apiVersion: apps/v1
kind: ReplicaSet
metadata:
  name: nginx-replicaset-demo
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx-server
  template:
    metadata:
      labels:
        app: nginx-server
    spec:
      containers:
        - name: nginx-demo
          image: nginx:1.19.4
          resources:
            requests:
              memory: "256Mi"
              cpu: "100m"
            limits:
              memory: "256Mi"
              cpu: "100m"
```

The result of executing this file with the command ```kubectl create -f nginx-replicaset.yml``` is:

![Alt](./resources/md_images/2-create-replicaset.png "Command output create replicaset")

**¿Cúal sería el comando que utilizarías para escalar el número de replicas a 10?**

```kubectl scale --replicas=10 -f nginx-replicaset.yml ```

Result:

![Alt](./resources/md_images/2-replicaset-scale-up.png "Command output replicaset scale up")

**Si necesito tener una replica en cada uno de los nodos de Kubernetes, ¿qué objeto se adaptaría mejor? (No es necesario 
adjuntar el objeto)**

A DaemonSet will create a pod in every node.