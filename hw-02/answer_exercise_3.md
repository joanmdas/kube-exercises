In this exercise we have 4 yml files that creates a service with different network configuration.

For starters, create a replicaset yml file like before:
```yaml
apiVersion: apps/v1
kind: ReplicaSet
metadata:
  name: nginx-replicaset-demo
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx-server
  template:
    metadata:
      labels:
        app: nginx-server
    spec:
      containers:
        - name: nginx-demo
          image: nginx:1.19.4
          ports:
            - containerPort: 80
          resources:
            requests:
              memory: "256Mi"
              cpu: "100m"
            limits:
              memory: "256Mi"
              cpu: "100m"
``` 

And execute the command to create it: ```kubectl create -f nginx-replicaset.yml```.

**Exponiendo el servicio hacia el exterior**

Create a service with a type of NodePort.
```yaml
apiVersion: v1
kind: Service
metadata:
  name: nginx-service-demo
  labels:
    name: nginx-service-demo
spec:
  type: NodePort
  selector:
    app: nginx-server
  ports:
    - protocol: TCP
      port: 80
```
Create the service with the command: ```kubectl create -f createService1.yml```.

![Alt](./resources/md_images/3-1-create-service.png "Command output create service")

To check the internal access, select a pod and execute the command "kubectl exec -it {POD_NAME} -- curl {CLUSTER_IP}:{INTERNAL_PORT}".
For example: ```kubectl exec -it nginx-replicaset-demo-8pnqw -- curl 10.110.249.51:80```

The response should be the welcome page of nginx.

![Alt](./resources/md_images/3-1-check-internal-access.png "Command output check internal access")

To check the access to the node, first we need the IP of the node. We will get it using this command: ```kubectl describe node```.
From all the printed information we get the "InternalIP" field from "Addresses" category.

![Alt](./resources/md_images/3-1-get-node-ip.png "Command output get node IP")

Afterwards we execute the command "kubectl exec -it {POD_NAME} -- curl {NODE_IP}:{EXTERNAL_PORT}" to check the external access:

![Alt](./resources/md_images/3-1-check-external-access.png "Command output check external access")

The external port can be obtained by executing the command ``` kubectl get svc {SERVICE_NAME}```.

![Alt](./resources/md_images/3-1-external-port.png "Command output get service port info")

**De forma interna, sin acceso desde el exterior**

Create a service with a type of ClusterIP. ClusterIP is the default type.
```yaml
apiVersion: v1
kind: Service
metadata:
  name: nginx-service-demo
spec:
  type: ClusterIP
  selector:
    app: nginx-server
  ports:
    - protocol: TCP
      port: 80
```
Start the service executing ```kubectl create -f createService2.yml```.

You can check the result with ```kubectl get service nginx-service-demo```.

![Alt](./resources/md_images/3-2-service-created.png "Command output created service")

As it can be seen, the type is now "ClusterIP", and the "PORT(S)" field only mentions one port instead of two. This port 
is the container port.

As before, to check the internal connection use the command "kubectl exec -it {POD_NAME} -- curl {CLUSTER_IP}:{INTERNAL_PORT}".
It returns the nginx start page.

![Alt](./resources/md_images/3-2-check-internal-acces.png "Command output check external access")
 
On the other hand, we can't check the external access as we don't have a node port available.

**Abriendo un puerto especifico de la VM**

To do that, you need a service with a type LoadBalancer. 
```yaml
apiVersion: v1
kind: Service
metadata:
  name: nginx-service-demo
  labels:
    name: nginx-service-demo
spec:
  type: LoadBalancer
  selector:
    app: nginx-server
  ports:
    - protocol: TCP
      port: 80
```

This will create an external IP that can be used to access the resource from outside the node. The problem is that when 
executing the command to create the service, the external IP ends up in a pending state. According to 
[this link](https://stackoverflow.com/questions/44110876/kubernetes-service-external-ip-pending), the 
problem is that minikube is being used and does not have LoanBalancer integrated. To solve this, we need a NodePort type 
service plus ingress controller.

Here is the yml of the service:
```yaml
apiVersion: v1
kind: Service
metadata:
  name: nginx-service-demo
  labels:
    name: nginx-service-demo
spec:
  type: NodePort
  selector:
    app: nginx-server
  ports:
    - protocol: TCP
      port: 80
```

We execute the command to create the service and check that it was created successfully:

![Alt](./resources/md_images/3-3-create-service.png "Command output create service")

Here is the yml for the ingress:
```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: nginx-ingress-demo
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /
spec:
  rules:
    - http:
        paths:
          - path: /demo
            pathType: Prefix
            backend:
              service:
                name: nginx-service-demo
                port:
                  number: 80
```
To create the ingress, we need to execute the command ```kubectl create -f createIngress3.yml```. We check the result by 
executing the following command: ```kubectl describe ingress nginx-ingress-demo```.

![Alt](./resources/md_images/3-3-create-ingress.png "Command output create ingress")

As seen in the image above, we get a field "Address" with an IP. To check that the pod is accessible from outside, 
we execute the command ```curl http://192.168.49.2/demo```. Note that this time we did not need to execute 
"kubectl exec -it ..." command.

![Alt](./resources/md_images/3-3-check-cli.png "Command output check cli")
