First we need to create a deployment with "Recreate" strategy. To do that we will use the following yml:
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment-demo
  labels:
    app: nginx-demo
spec:
  replicas: 3
  strategy:
    type: Recreate
  selector:
    matchLabels:
      app: nginx-server
  template:
    metadata:
      labels:
        app: nginx-server
    spec:
      containers:
        - name: nginx-demo
          image: nginx:1.19.4
          resources:
            requests:
              memory: "256Mi"
              cpu: "100m"
            limits:
              memory: "256Mi"
              cpu: "100m"
```
After executing the command to create the deployment, we check the result:

![Alt](./resources/md_images/4-create-deployment.png "Command output create deployment") 

Following is the result of the "describe" command on the deployment:

![Alt](./resources/md_images/4-create-describe.png "Command output describe deployment") 

Now we will update the nginx image of the container to 1.19.4-alpine. To do so we execute the command 
```kubectl set image deployment.apps/nginx-deployment-demo nginx-demo=nginx:1.19.4-alpine --record```.

If we once again execute the "describe" option for the deployment we will get the following output:

![Alt](./resources/md_images/4-deployment-update.png "Command output deployment update") 

As it can be seen, the revision went up to 2, the container image was updated to the alpine version, and the events show 
that the replicaset scaled down to 0 and afterwards went up to 3. There was a moment were no pods for the replicaset were up.

Now, in order to rollback to the previous revision, execute the following command: 
```kubectl rollout undo deployment.apps/nginx-deployment-demo --to-revision=1```.

The ```--to-revision=1``` is optional if you want to go to the previous revision.

Following is the result of the rollback:

![Alt](./resources/md_images/4-deployment-rollback.png "Command output deployment rollback") 

As it can be seen, the revision went up to three, the image went back to the 1.19.4 version, and the events show how the 
replicaset went back to 0 to once again 3 replicas.