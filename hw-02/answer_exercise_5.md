To design a blue/green deployment, we will need three yml files: one containing the blue deployment, another containing 
the green deployment and finally a service pointing to the blue deployment.

The blue deployment:
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment-demo-blue
  labels:
    app: nginx-demo
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx-server
  template:
    metadata:
      labels:
        app: nginx-server
        role: blue
    spec:
      containers:
        - name: nginx-demo
          image: nginx:1.19.4
          ports:
            - containerPort: 80
          readinessProbe:
            httpGet:
              path: /
              port: 80
          resources:
            requests:
              memory: "256Mi"
              cpu: "100m"
            limits:
              memory: "256Mi"
              cpu: "100m"
```

The green deployment:
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment-demo-green
  labels:
    app: nginx-demo
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx-server
  template:
    metadata:
      labels:
        app: nginx-server
        role: green
    spec:
      containers:
        - name: nginx-demo
          image: nginx:1.19.4-alpine
          ports:
            - containerPort: 80
          readinessProbe:
            httpGet:
              path: /
              port: 80
          resources:
            requests:
              memory: "256Mi"
              cpu: "100m"
            limits:
              memory: "256Mi"
              cpu: "100m"
```

The service with the role "blue":
```yaml
apiVersion: v1
kind: Service
metadata:
  name: nginx-service-demo
  labels:
    name: nginx-service-demo
    role: blue
spec:
  type: NodePort
  selector:
    app: nginx-server
    role: blue
  ports:
    - protocol: TCP
      port: 80
```

As it can be seen, all share a label "role".

The result of creating the two deployments and the service is the following:

![Alt](./resources/md_images/5-elements-created.png "Command output environment created")

As seen, the green deployment, replicaset and pods are running but not ready. Also, the service only has three endpoints 
defined.

The next step involves modifying the "role" fields from "blue" to "green" in the "nginx-service.yml" file. To apply this 
change to the kubernetes, execute the command ```kubectl apply -f nginx-service.yml```.

The service with the role "green":
```yaml
apiVersion: v1
kind: Service
metadata:
  name: nginx-service-demo
  labels:
    name: nginx-service-demo
    role: green
spec:
  type: NodePort
  selector:
    app: nginx-server
    role: green
  ports:
    - protocol: TCP
      port: 80
```

The result of applying the changes is the following:

![Alt](./resources/md_images/5-green-deployment.png "Command output green deployment")

Then we can delete the blue deployment and check that the service still has the same three endpoints attached.

![Alt](./resources/md_images/5-final.check.png "Command output final check")