# 1.a

To achieve this, we use 3 yml files.

One for the replicaset: 
```yaml
apiVersion: apps/v1
kind: ReplicaSet
metadata:
  name: nginx-replicaset-demo
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx-server
  template:
    metadata:
      labels:
        app: nginx-server
    spec:
      containers:
        - name: nginx-demo
          image: nginx:1.19.4
          ports:
            - containerPort: 80
          resources:
            requests:
              memory: "128Mi"
              cpu: "20m"
            limits:
              memory: "128Mi"
              cpu: "20m"
```

One for the service:
```yaml
apiVersion: v1
kind: Service
metadata:
  name: nginx-service-demo
  labels:
    name: nginx-service-demo
spec:
  selector:
    app: nginx-server
  ports:
    - protocol: TCP
      port: 80
```

And one for the Ingress:
```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: nginx-ingress-demo
  annotations:
    kubernates.io/ingress.class: nginx
    nginx.ingress.kubernetes.io/ssl-redirect: "false"
spec:
  defaultBackend:
    service:
      name: nginx-service-demo
      port:
        number: 80
  rules:
    - host: joan.student.lasalle.com
      http:
        paths:
          - path: /
            pathType: Prefix
            backend:
              service:
                name: nginx-service-demo
                port:
                  number: 80
```

After creating this 3 resources, we get the following:

![Alt](./resources/md_images/1-a-check-resources.png "Command output check resources")

![Alt](./resources/md_images/1-a-describe-ingress.png "Command output check resources")

And when we use the curl command  ```curl -k http://joan.student.lasalle.com``` we get the following:

![Alt](./resources/md_images/1-a-use-curl.png "Command output use curl")

#1.b

First we need to create a certificate with openssl with the command ```openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout tls.key -out tls.crt```.
When we get the files, we need to obtain the base64 string that will be used in the secret configuration. to do that we 
execute the commands ```cat tls.crt | base64``` and ```cat tls.key | base64```.

To create a TLS secret we use the following yml to create it:
```yaml
apiVersion: v1
kind: Secret
metadata:
  name: nginx-secret-demo
  namespace: default
data:
  tls.crt: LS0tLS...
  tls.key: LS0tLS...
type: kubernetes.io/tls
```

Execute the command ```kubectl create -f nginx-secret.yml``` to add it.

We also need to add the TLS secret to the ingress resource:

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: nginx-ingress-ssl-demo
  annotations:
    kubernates.io/ingress.class: nginx
    nginx.ingress.kubernetes.io/ssl-redirect: "true"
spec:
  tls:
    - hosts:
        - joan.student.lasalle.com
      secretName: nginx-secret-demo
  defaultBackend:
    service:
      name: nginx-service-demo
      port:
        number: 80
  rules:
    - host: joan.student.lasalle.com
      http:
        paths:
          - path: /
            pathType: Prefix
            backend:
              service:
                name: nginx-service-demo
                port:
                  number: 80
```

After creating them we have the following:

![Alt](./resources/md_images/1-b-check-resources.png "Command output check resources")

When we try the curl command with ssl ```curl --cacert tls.crt https://joan.student.lasalle.com```, we get:

![Alt](./resources/md_images/1-b-use-curl-ssl.png "Command output use curl ssl")

But now, if we try the command without ssl it will fail.

![Alt](./resources/md_images/1-b-use-curl.png "Command output use curl")

