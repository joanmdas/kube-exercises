To create a mongodb in order to test statefulset, we use the following yml:
```yaml
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: mongo-statefulset-demo
spec:
  replicas: 3
  selector:
    matchLabels:
      role: mongodb
      environment: test
  serviceName: mongodb-service-demo
  template:
    metadata:
      labels:
        role: mongodb
        environment: test
    spec:
      terminationGracePeriodSeconds: 5
      containers:
        - name: mongo
          image: mongo:3.6.21-xenial
          command:
            - mongod
            - "--bind_ip=0.0.0.0"
            - "--replSet=rs0"
            - "--smallfiles"
            - "--noprealloc"
          ports:
            - containerPort: 27017
          resources:
            requests:
              memory: "256Mi"
              cpu: "100m"
            limits:
              memory: "256Mi"
              cpu: "100m"
        - name: mongo-sidecar
          image: cvallance/mongo-k8s-sidecar
          env:
            - name: MONGO_SIDECAR_POD_LABELS
              value: "role=mongo,environment=test"
```
This is also accompanied by a service.
```yaml
apiVersion: v1
kind: Service
metadata:
  name: mongodb-service-demo
  labels:
    name: mongodb-service-demo
spec:
  selector:
    role: mongodb
  ports:
    - port: 27017
      targetPort: 27017
  clusterIP: None
```

The result is as follows:

![Alt](./resources/md_images/2-check-resources.png "Command output check resources")

If we get inside one of the instances with the command ```kubectl exec -it pod/mongo-statefulset-demo-0 mongo``` and 
execute the command ```rs.status()``` we find that the mongodb replica has not been initialized.

![Alt](./resources/md_images/2-rs-status-first.png "Command output check mongo replicaset")

To initialize it, we need to execute the following command: 
```
rs.initiate({_id: "rs0", version: 1, members: [
 { _id: 0, host : "mongo-statefulset-demo-0.mongodb-service-demo.default.svc.cluster.local:27017" },
 { _id: 1, host : "mongo-statefulset-demo-1.mongodb-service-demo.default.svc.cluster.local:27017" },
 { _id: 2, host : "mongo-statefulset-demo-2.mongodb-service-demo.default.svc.cluster.local:27017" }
]});
```

To do so we first need to get the FQDN of the pod. It can be done by entering inside the pod with 
```kubectl exec -it pod/mongo-statefulset-demo-0 -- sh``` and execute the command ```hostname -f```.

![Alt](./resources/md_images/2-pod-hostname.png "Command output check mongo replicaset")

If we once again execute the command ```rs.status()``` after initializing the replicaset, we obtain the following result:

![Alt](./resources/md_images/2-rs-status-second.png "Command output check mongo replicaset")

Then, if we enter into another pod we can check if the changes from one pod had been applied to another pod. To do so, 
we execute the command ```kubectl exec -it pod/mongo-statefulset-demo-0 -- mongo``` and execute ```rs.status()```.

![Alt](./resources/md_images/2-rs-status-another-pod.png "Command output check mongo replicaset")

As we can see, the changes had been applied successfully.

--

A replicaset, when scaling up and down it does this at once, it tries to create/delete all the new instances at the same 
time. Meanwhile, a statefulset will do so one at a time, it will wait for one instance to created/deleted before going for 
a new one. Another difference is that the naming of the resource will have a random part in the replicaset, while in the 
statefulset the name will have a no random part in the name. Take for example the names in the replicaset like 
```nginx-replicaset-demo-[random-number]``` and ones in the statefullset ```mongo-statefulset-demo-[number]```. This is 
helpful when an instance stops, and a new one needs to be created. In the following example we will recreate the failure 
of the instance ```mongo-statefulset-demo-1```.

First we delete the pod.

![Alt](./resources/md_images/2-delete-pod.png "Command output delete mongo instance")

When we go and check the resources, we can see that a new pod with the **same name** as the one that had benn deleted 
(it has a life of 15 seconds instead of the 22 minutes of the others).

![Alt](./resources/md_images/2-pod-same-name.png "Command output check new pod")

Viewing the events of the statefullset we can see there is one for the creation of a new ```mongo-statefulset-demo-1```.

![Alt](./resources/md_images/2-statefulset-event-new-pod.png "Command output check event new pod")

The previous workflow in a replicaset will have generated a new name due to the random part ```nginx-replicaset-demo-[random-number]```.