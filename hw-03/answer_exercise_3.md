To test an HPA we need to create a deployment, a service and a hpa. Following are the configuration for those.

Deployment
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment-demo
  labels:
    app: nginx-demo
spec:
  replicas: 2
  selector:
    matchLabels:
      app: nginx-server
  template:
    metadata:
      labels:
        app: nginx-server
    spec:
      containers:
        - name: nginx-demo
          image: nginx:1.19.4-alpine
          ports:
            - containerPort: 80
          readinessProbe:
            httpGet:
              path: /
              port: 80
          resources:
            requests:
              memory: "64Mi"
              cpu: "20m"
            limits:
              memory: "64Mi"
              cpu: "20m"
``` 

Service
```yaml
apiVersion: v1
kind: Service
metadata:
  name: nginx-service-demo
  labels:
    name: nginx-service-demo
spec:
  selector:
    app: nginx-server
  ports:
    - protocol: TCP
      port: 80
```

HPA:
```yaml
apiVersion: autoscaling/v2beta2
kind: HorizontalPodAutoscaler
metadata:
  name: nginx-hpa-demo
spec:
  scaleTargetRef:
    apiVersion: apps/v1
    kind: Deployment
    name: nginx-deployment-demo
  minReplicas: 2
  maxReplicas: 6
  metrics:
    - type: Resource
      resource:
        name: cpu
        target:
          type: Utilization
          averageUtilization: 50
    - type: Resource
      resource:
        name: memory
        target:
          type: Utilization
          averageUtilization: 50
```

The result is as follows:

![Alt](./resources/md_images/3-check-resources.png "Command output check resources")

As stated in the exercise, we limited the cpu and memory to 50% load in order to increase/decrease replicas.

In order to test the scalability we executed the command 
```kubectl run -i --tty load-generator --rm --image=busybox --restart=Never -- /bin/sh -c "while sleep 0.01; do wget -q -O- http://nginx-service-demo; done"```.

As we can see, once it reached the 50% threshold, a new replica was created and once the load receded it turned back to 
its original state of 2 replicas.

![Alt](./resources/md_images/3-check-hpa.png "Command output check hpa")